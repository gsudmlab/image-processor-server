/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab;

import java.io.IOException;
import java.util.TimeZone;

import edu.gsu.cs.dmlab.config.ConfigReader;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;
import edu.gsu.cs.dmlab.service.NioSocketServer;

/**
 * Main class of the ParamCalcServer project. When compiled into a jar this is
 * where the program execution starts.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ParamCalcServer {

	private static ConfigReader config;

	/**
	 * Main method of the program. Arguments can be passed in to specify where the
	 * config file is located. If 1 is passed in, it is assumed that the argument is
	 * the directory that contains the config file, and the config file is assumed
	 * to be named calcserver.cfg.xml. If 2 arguments are passed in, the first is
	 * assumed to be the directory for the config file, and the second is assumed to
	 * be the config file name. If no arguments are passed in, the config file is
	 * assumed to be in a directory relative to the execution location named config
	 * with a config file named calcserver.cfg.xml.
	 * 
	 * @param args The arguments, can be length 0, 1, or 2.
	 * 
	 * @throws Exception
	 */
	public static void main(String[] args) throws InterruptedException, IOException {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		setConfig(args);

		NioSocketServer server = new NioSocketServer(config);
		server.run();
	}

	private static void setConfig(String[] args) {
		try {

			String configFolder = "config";
			String configName = "calcserver.cfg.xml";
			if (args.length == 1) {
				configFolder = args[0];
			} else if (args.length == 2) {
				configFolder = args[0];
				configName = args[1];
			}
			config = new ConfigReader(configFolder, configName);

		} catch (InvalidConfigException e) {
			System.out.println("======================================================");
			System.out.println("Cannot configure project.");
			System.out.println("Is the config file located in the ./config directory?");
			System.out.println("\n\n");
			System.out.println("==================Stack Trace=========================");
			e.printStackTrace();
			System.out.println("======================================================");
			System.exit(1);
		}

	}

}
