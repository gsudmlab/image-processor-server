/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service.io;

import java.io.ByteArrayInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

import edu.gsu.cs.dmlab.datatypes.ParamResults;
import edu.gsu.cs.dmlab.service.interfaces.IDBService;

/**
 * The class that completes handling of a connection taht is returning results
 * from a worker process.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ReadResultsHandler implements CompletionHandler<Integer, Map<String, Object>> {

	IDBService dbService;
	Logger logger;

	/**
	 * Constructor
	 * 
	 * @param dbService The database service that handles saving the results to the
	 *                  database.
	 */
	public ReadResultsHandler(IDBService dbService, Logger logger) {
		this.dbService = dbService;
		this.logger = logger;
	}

	@Override
	public void completed(Integer result, Map<String, Object> attachment) {

		ArrayList<byte[]> inputValues = new ArrayList<byte[]>();
		boolean readSuccess = false;
		try (AsynchronousSocketChannel ch = (AsynchronousSocketChannel) attachment.get("channel")) {
			ByteBuffer buffer = (ByteBuffer) attachment.get("buffer");

			boolean running = true;
			int bytesRead = result;

			while (bytesRead != -1 && running) {
				if (buffer.position() > 2) {

					((Buffer) buffer).flip();

					byte[] lineBytes = new byte[bytesRead];
					buffer.get(lineBytes, 0, bytesRead);
					inputValues.add(lineBytes);
					((Buffer) buffer).clear();
					bytesRead = ch.read(buffer).get(20, TimeUnit.SECONDS);
				} else {
					// An empty line signifies the end.
					running = false;
				}
			}
			readSuccess = true;
		} catch (Exception e) {
			logger.error("Failure while reading channel", e);
		}
		attachment.clear();

		if (readSuccess) {
			int totalBytes = 0;
			for (byte[] b : inputValues)
				totalBytes += b.length;

			byte[] allBytes = new byte[totalBytes];
			int idx = 0;
			for (int i = 0; i < inputValues.size(); i++) {
				byte[] tmpBytes = inputValues.get(i);
				for (int j = 0; j < tmpBytes.length; j++) {
					allBytes[idx++] = tmpBytes[j];
				}
			}

			inputValues.clear();
			inputValues = null;

			try {
				ParamResults results = null;
				try (ByteArrayInputStream bis = new ByteArrayInputStream(allBytes)) {
					try (ObjectInput in = new ObjectInputStream(bis)) {
						results = (ParamResults) in.readObject();
					}
				}

				if (results != null)
					this.dbService.saveResults(results);

			} catch (Exception e) {
				logger.error("Failure while attempting to decode object.", e);
			}
		}
	}

	@Override
	public void failed(Throwable exc, Map<String, Object> attachment) {
		attachment.clear();
		logger.error("Failure befor handling connection results", exc);
	}

}
