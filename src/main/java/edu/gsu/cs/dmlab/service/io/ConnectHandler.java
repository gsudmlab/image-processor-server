/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.Buffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;

import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.service.interfaces.IDBService;

/**
 * The class that handles when a connection is made to this process. It does
 * various back and forth with the requesting process to determine what to do.
 * I.E. is the process requesting to send this process results, or is it
 * requesting another descriptor of an image it should process. Once that is
 * determined, the proper action is taken.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ConnectHandler implements CompletionHandler<AsynchronousSocketChannel, Void> {

	private AsynchronousServerSocketChannel listener;
	private ReadResultsHandler readResultHandler;
	private IDBService dbService;

	private Lock loc;
	private Logger logger;

	/**
	 * Constructor
	 * 
	 * @param listener          The listener that is listening for connections and
	 *                          this handler is to handle connections for.
	 * 
	 * @param readResultHandler The completion handler object for handling returned
	 *                          results by reading them into an object and what not.
	 * 
	 * @param dbService         The database service that returns image descriptors
	 *                          that shall be sent to the requesting process.
	 * 
	 * @param logger            A logger that logs the exceptions and some info,
	 *                          that way we can keep an eye on the goings on.
	 */
	public ConnectHandler(AsynchronousServerSocketChannel listener, ReadResultsHandler readResultHandler,
			IDBService dbService, Logger logger) {

		this.listener = listener;
		this.readResultHandler = readResultHandler;
		this.dbService = dbService;
		this.logger = logger;
		this.loc = new ReentrantLock();
	}

	@Override
	public void completed(AsynchronousSocketChannel ch, Void att) {

		// Accept the next connection
		this.loc.lock();
		if (listener.isOpen()) {
			listener.accept(null, this);
		}
		this.loc.unlock();
		try {
			if ((ch != null) && ch.isOpen()) {

				// Allocate a byte buffer (4) to read from the client
				ByteBuffer byteBuffer = ByteBuffer.allocate(4);

				// Read the first line
				int bytesRead = ch.read(byteBuffer).get(20, TimeUnit.SECONDS);

				// Make sure that we have data to read
				if (byteBuffer.position() == 1) {

					// Make the buffer ready to read
					// Had to add cast buffer for compile with > 1.8 jdk and 1.8 target
					((Buffer) byteBuffer).flip();

					// Convert the buffer into a line
					byte[] lineBytes = new byte[bytesRead];
					byteBuffer.get(lineBytes, 0, bytesRead);

					// Parse what code the client sent when connecting
					int eventId = Integer.parseInt(new String(lineBytes));

					// If code #1 then the client wants another image id to process
					if (eventId == 1) {

						try {
							// Reply back to the client with the same code as was
							// sent. It is just the protocol that I decided on.
							byteBuffer = ByteBuffer.wrap(lineBytes);
							Future<Integer> writeResult = ch.write(byteBuffer);
							writeResult.get(20, TimeUnit.SECONDS);

							ParamId paramId = this.dbService.getNext();
							if (paramId != null) {
								try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
									try (ObjectOutput out = new ObjectOutputStream(bos)) {
										out.writeObject(paramId);
										out.flush();
									}
									byte[] byteArr = bos.toByteArray();

									ByteBuffer buff = ByteBuffer.wrap(byteArr);
									writeResult = ch.write(buff);
									writeResult.get(20, TimeUnit.SECONDS);
								}
								paramId = null;

							}
						} catch (Exception e) {
							logger.error("Error occured while attempting to process image id request.", e);
						} finally {
							// This path we want to close the channel for all paths
							if ((ch != null) && ch.isOpen())
								try {
									ch.close();
								} catch (IOException e) {
									// Do nothing here, it's not an exception of import.
								}
						}

					} else if (eventId == 2) {
						try {
							// Reply back to the client with the same code as was
							// sent. It is just the protocol that I decided on.
							byteBuffer = ByteBuffer.wrap(lineBytes);
							Future<Integer> writeResult = ch.write(byteBuffer);
							writeResult.get(20, TimeUnit.SECONDS);

							ByteBuffer byteBuffer2 = ByteBuffer.allocate(4096);
							Map<String, Object> info = new HashMap<>();
							info.put("buffer", byteBuffer2);
							info.put("channel", ch);

							ch.read(byteBuffer2, info, this.readResultHandler);
							// Don't want to close during normal operation as the read
							// handler closes the channel.
						} catch (Exception e) {
							// We only want to close the channel when an error occurs here
							logger.error("Error while attempting to process returned results.", e);
							if ((ch != null) && ch.isOpen())
								try {
									ch.close();
								} catch (IOException e2) {
									// Do nothing here, it's not an exception of import.
								}
						}
					} else {
						// If we get here, something is incorrect, just close the channel
						logger.error("Improper Protocol Initiation, though something was passed.");
						if ((ch != null) && ch.isOpen())
							try {
								ch.close();
							} catch (IOException e) {
								// Do nothing here, it's not an exception of import.
							}
					}
				} else {
					// If we get here, something is incorrect, just close the channel
					logger.error("Improper Protocol Initiation, nothing was passed.");
					if ((ch != null) && ch.isOpen())
						try {
							ch.close();
						} catch (IOException e) {
							// Do nothing here, it's not an exception of import.
						}
				}

			}
		} catch (Exception e) {
			logger.error("Failure before initial read", e);
			// If an exception is thrown on the initial read, just make sure the
			// channel is closed.
			if ((ch != null) && ch.isOpen())
				try {
					ch.close();
				} catch (IOException e2) {
					// Do nothing here, it's not an exception of import.
				}
		}
	}

	@Override
	public void failed(Throwable arg0, Void arg1) {
		logger.error("Failed to handle connection.", arg0);
	}

}
