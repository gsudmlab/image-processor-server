/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2020 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.slf4j.Logger;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;

import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBCreator;
import edu.gsu.cs.dmlab.factories.interfaces.IPopulatorFactory;
import edu.gsu.cs.dmlab.service.interfaces.IDBSaveTasksSupervisor;
import edu.gsu.cs.dmlab.service.interfaces.IListPopulatorTasksSupervisor;

public class CyclingServerTaskSupervisor extends BaseDBService
		implements IListPopulatorTasksSupervisor, IDBSaveTasksSupervisor {

	private Logger logger;

	private DateTime epoc;
	private DateTime endTime;
	private DateTime currentStartPosition;
	private DateTime lastPosition;

	private boolean isRunning = false;

	private IISDStateDBConnector stateDBConnector;
	private ListeningExecutorService connectionHandleExecutor;

	/**
	 * Constructor for the service that handles the tasks of populating the dataset
	 * in a cycle from epoch to a set end time until stopped.
	 * 
	 * @param populatorFactory      The factory object that produces all the objects
	 *                              used in this class.
	 * 
	 * @param stateDbCreator        The DB connection object that is used to make
	 *                              the state tables in the database.
	 * 
	 * @param stateDBConnector      The DB connection object that is used to update
	 *                              the state tables in the database.
	 * 
	 * @param connectHandleExecutor The executor object used to run tasks that will
	 *                              handle connection requests.
	 * 
	 * @param saveExecutor          The executor object used to run tasks that will
	 *                              save data to the database.
	 * 
	 * @param epoc                  The global start time used as the indicator of
	 *                              where data is first available for processing.
	 *                              This is only used when cycling through the
	 *                              entire dataset or when this process is run for
	 *                              the first time and the state tables have yet to
	 *                              have information about the last processed time.
	 * 
	 * @param endTime               The global end time used as the indicator of
	 *                              where data stops being available for processing.
	 *                              This is only used when cycling through the
	 *                              entire database.
	 * 
	 * @param maxConcurrent         The limit on the maximum concurrent insert tasks
	 *                              can be running prior to this process stops
	 *                              sending out new image descriptors to requesting
	 *                              worker processes.
	 * 
	 * @param logger                The logging object that will log error
	 *                              information.
	 */
	public CyclingServerTaskSupervisor(IPopulatorFactory populatorFactory, IISDStateDBCreator stateDbCreator,
			IISDStateDBConnector stateDBConnector, ListeningExecutorService connectHandleExecutor,
			ListeningExecutorService saveExecutor, DateTime epoc, DateTime endTime, int maxConcurrent, Logger logger) {
		super(populatorFactory, saveExecutor, maxConcurrent, logger);

		this.connectionHandleExecutor = connectHandleExecutor;
		this.stateDBConnector = stateDBConnector;
		this.logger = logger;

		this.epoc = epoc;
		this.endTime = endTime;
		// Initialize the last position that was run variable with the epoc so it
		// will be updated later.
		this.lastPosition = new DateTime(this.epoc);
		this.currentStartPosition = new DateTime(this.epoc);

		try {
			if (!stateDbCreator.checkStateTableExists()) {
				stateDbCreator.createStateTable();
			}
			this.stateDBConnector.setFinishedProcessing(null);

			this.runListPopulate();
		} catch (SQLException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleResultsStored(Boolean done) {
		try {
			if (this.saveLock.tryLock() || this.saveLock.tryLock(20, TimeUnit.SECONDS)) {
				// Cycle through the list of reporting tasks and remove all that are done.
				// We are only using the lists to count how many are in process in some way.
				// We will then determine if we want to return an id based on how many are still
				// in process.
				Iterator<Future<Boolean>> itr = this.reportingTaskList.iterator();
				while (itr.hasNext()) {
					Future<Boolean> tsk = itr.next();
					if (tsk.isDone()) {
						itr.remove();
					}
				}

			}
		} catch (Exception e) {
			this.logger.error("Exception occurred while executing method handleResultsStored", e);
		} finally {
			if (this.saveLock.isHeldByCurrentThread())
				this.saveLock.unlock();
		}
	}

	@Override
	public void handleResultsStoredFailed(Throwable arg0) {
		try {
			if (this.saveLock.tryLock() || this.saveLock.tryLock(20, TimeUnit.SECONDS)) {
				// Cycle through the list of reporting tasks and remove all that are done.
				// We are only using the lists to count how many are in process in some way.
				// We will then determine if we want to return an id based on how many are still
				// in process.
				Iterator<Future<Boolean>> itr = this.reportingTaskList.iterator();
				while (itr.hasNext()) {
					Future<Boolean> tsk = itr.next();
					if (tsk.isDone()) {
						itr.remove();
					}
				}
			}
		} catch (Exception e) {
			this.logger.error("Exception occurred while executing method handleResultsStored", e);
		} finally {
			if (this.saveLock.isHeldByCurrentThread())
				this.saveLock.unlock();
		}
	}

	@Override
	public void handleParamIdListPopulated(DateTime done) {
		try {
			if (this.lock.tryLock() || this.lock.tryLock(20, TimeUnit.SECONDS)) {
				this.logger.info("Success of RunPopulate: " + this.currentStartPosition.toString());

				DateTime time;
				// There is also an expected end time for the cycle to start over and go back to
				// the beginning. However, if the user didn't supply that, then we will assume
				// it is whatever time it happens to be now.
				if (this.endTime == null)
					time = new DateTime();
				else
					time = new DateTime(this.endTime);

				// We get a time that is relative to the current position of our processing. The
				// current time should not be null here because starting the list populating
				// process should have set this variable. This time should be set to the first
				// of the next month that we wish to process, because this list populating
				// process should have finished at the end of the month.
				DateTime tmpTime = this.currentStartPosition.plusMonths(1);
				this.currentStartPosition = new DateTime(tmpTime.getYear(), tmpTime.getMonthOfYear(), 1, 0, 0, 0);

				// Update the state db with the latest time that the list population task was
				// started from. This is the only time we know was processed up to.
				if (this.currentStartPosition.isAfter(this.lastPosition))
					this.stateDBConnector.updateLastProcessedTime(null, this.lastPosition);

				// If our next starting time is after the calculated end time, then that means
				// we need to start over on the cycling through the dataset from the epoc.
				if (this.currentStartPosition.isAfter(time)) {
					this.currentStartPosition = new DateTime(this.epoc);
				}

			}
		} catch (Exception e) {
			this.logger.error("Exception occurred while executing method handleParamIdListPopulated", e);
		} finally {
			this.isRunning = false;
			if (this.lock.isHeldByCurrentThread())
				this.lock.unlock();
		}
	}

	@Override
	public void handleParamIdListPopulatedFailed(Throwable arg0) {
		// this method is to handle the failure of the id list populating task.
		// We will only try to be nice and lock before we change the state.
		this.lock.tryLock();
		this.isRunning = false;
		this.logger.error("Exception occurred while executing Populate Param List.", arg0);
		if (this.lock.isHeldByCurrentThread()) {
			this.lock.unlock();
		}
	}

	@Override
	protected void runListPopulate() {
		if (!this.isRunning) {
			try {
				// Check to make sure we are ok to run, maybe something is blocking us.
				if (this.stateDBConnector.checkOKToProcess(null)) {
					this.stateDBConnector.setIsProcessing(null);

					try {
						// Get a new task that populates the list of images that need to be processed.
						Callable<DateTime> populateList = this.populatorFactory.getPopulatorTask(this.idsList,
								this.currentStartPosition);

						// Submit the task to the executor and then add a callback to this supervisor
						// for handling when it completes.
						ListenableFuture<DateTime> future = this.connectionHandleExecutor.submit(populateList);
						Futures.addCallback(future, this.populatorFactory.getPopulatorTaskCallback(this),
								this.connectionHandleExecutor);

						// If our current starting position is after the last run position, then we need
						// to update the last run position variable with the current run start.
						if (this.currentStartPosition.isAfter(this.lastPosition)) {
							this.lastPosition = new DateTime(this.currentStartPosition);
						}

						// State we are running the populate task.
						this.isRunning = true;
					} catch (Exception e) {
						this.logger.error("Interrupted Exception occurred while executing method populateIdList", e);
						this.isRunning = false;
					}
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	protected IDBSaveTasksSupervisor getSaveTaskSupervisor() {
		return this;
	}

}
