package edu.gsu.cs.dmlab.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;

import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.ParamResults;
import edu.gsu.cs.dmlab.factories.interfaces.IPopulatorFactory;
import edu.gsu.cs.dmlab.service.interfaces.IDBSaveTasksSupervisor;
import edu.gsu.cs.dmlab.service.interfaces.IDBService;

public abstract class BaseDBService implements IDBService {

	protected IPopulatorFactory populatorFactory;

	protected LockingLinkedList idsList;
	protected LinkedList<Future<Boolean>> reportingTaskList;

	protected ReentrantLock lock;
	protected ReentrantLock saveLock;

	private Logger logger;
	private ListeningExecutorService saveExecutor;

	private int maxConcurrent;

	@SuppressWarnings("serial")
	private class LockingLinkedList extends LinkedList<ParamId> {

		private ReentrantLock listlock;
		private Map<String, ParamId> map;

		public LockingLinkedList() {
			super();
			this.listlock = new ReentrantLock();
			this.map = new HashMap<String, ParamId>();
		}

		@Override
		public void push(ParamId e) {
			String key = "" + e.wavelength + e.id + e.time;

			try {
				if (this.listlock.tryLock() || this.listlock.tryLock(5, TimeUnit.SECONDS)) {
					if (!this.map.containsKey(key)) {
						super.push(e);
						this.map.put(key, e);
					}
				}
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			} finally {
				if (this.listlock.isHeldByCurrentThread())
					this.listlock.unlock();
			}

		}

		@Override
		public boolean add(ParamId e) {
			String key = "" + e.wavelength + e.id + e.time;
			boolean result = false;

			try {
				if (this.listlock.tryLock() || this.listlock.tryLock(5, TimeUnit.SECONDS)) {
					if (!this.map.containsKey(key)) {
						result = super.add(e);
						this.map.put(key, e);
					}
				}
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			} finally {
				if (this.listlock.isHeldByCurrentThread())
					this.listlock.unlock();
			}
			return result;
		}

		@Override
		public ParamId poll() {
			ParamId result = null;

			try {
				if (this.listlock.tryLock() || this.listlock.tryLock(5, TimeUnit.SECONDS)) {
					result = super.poll();
					if (result != null) {
						String key = "" + result.wavelength + result.id + result.time;
						if (this.map.containsKey(key))
							this.map.remove(key);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (this.listlock.isHeldByCurrentThread())
					this.listlock.unlock();
			}
			return result;

		}

		@Override
		public boolean isEmpty() {
			boolean result = false;
			try {
				if (this.listlock.tryLock() || this.listlock.tryLock(5, TimeUnit.SECONDS))
					result = super.isEmpty();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (this.listlock.isHeldByCurrentThread())
					this.listlock.unlock();
			}
			return result;
		}
	}

	/**
	 * Constructor for the base class of the service that handles the tasks of
	 * populating the dataset.
	 * 
	 * @param populatorFactory The factory object that produces all the objects used
	 *                         in this class.
	 * 
	 * @param saveExecutor     The executor object used to run tasks that will save
	 *                         data to the database.
	 * 
	 * @param maxConcurrent    The limit on the maximum concurrent insert tasks can
	 *                         be running prior to this process stops sending out
	 *                         new image descriptors to requesting worker processes.
	 * 
	 * @param logger           The logging object that will log error information.
	 */
	public BaseDBService(IPopulatorFactory populatorFactory, ListeningExecutorService saveExecutor, int maxConcurrent,
			Logger logger) {

		this.populatorFactory = populatorFactory;
		this.saveExecutor = saveExecutor;

		this.idsList = new LockingLinkedList();
		this.reportingTaskList = new LinkedList<Future<Boolean>>();

		this.lock = new ReentrantLock();
		this.saveLock = new ReentrantLock();

		this.maxConcurrent = maxConcurrent;
		this.logger = logger;
	}

	protected abstract void runListPopulate();

	protected abstract IDBSaveTasksSupervisor getSaveTaskSupervisor();

	@Override
	public ParamId getNext() {
		ParamId result = null;
		try {
			// First we need to try and lock access to the state variables. If we cannot,
			// then it means that something is already in the process of running and it too
			// wanted to lock access to the state variables. We will wait for 20 seconds and
			// try again some other time if we cannot lock the access in that time.
			if (this.lock.tryLock() || this.lock.tryLock(20, TimeUnit.SECONDS)) {

				// Now that we have exclusive access to the state variables and the id list, we
				// want to check if the id list is empty
				if (this.idsList.isEmpty()) {
					// If it is empty, run the list populate task based on whatever rules the
					// implementation class has for doing this.
					this.runListPopulate();
				}

				// The else is where the list isn't empty, so we aren't going to bother with
				// attempting to determine what state we are in and/or re-populating the list.

				// Now cycle through the list of reporting tasks and remove all that are done.
				// These tasks are those that are either inserting data returned from the
				// workers, or are waiting to insert data returned from the workers.
				//
				// We are only using the lists to count how many are in process in some way.
				// We will determine if we want to return an id based on how many are still
				// in process.
				try {
					if (this.saveLock.tryLock()) {
						Iterator<Future<Boolean>> itr = this.reportingTaskList.iterator();
						while (itr.hasNext()) {
							Future<Boolean> tsk = itr.next();
							if (tsk.isDone()) {
								itr.remove();
							}
						}
					}
				} finally {
					if (this.saveLock.isHeldByCurrentThread())
						this.saveLock.unlock();
				}

				// Here we check to see if there are too many tasks currently waiting to save
				// and if there are, we won't return anything for this request (I.E. null)
				if (this.reportingTaskList.size() < this.maxConcurrent) {
					// If list is empty, it will return null, which is how we indicate nothing to
					// return.
					//
					// If the list is not empty, it will return the head item.
					result = this.idsList.poll();
				}
			}

		} catch (Exception e) {
			this.logger.error("Interrupted Exception occurred while executing method getNext", e);
		} finally {
			if (this.lock.isHeldByCurrentThread())
				this.lock.unlock();
		}
		return result;
	}

	@Override
	public void saveResults(ParamResults results) {
		try {
			// First we need to try and lock access to the state variables. If we cannot,
			// then it means that something is already in the process of running and it too
			// wanted to lock access to the state variables. We will wait for 20 seconds and
			// try again some other time if we cannot lock the access in that time.
			if (this.lock.tryLock() || this.lock.tryLock(20, TimeUnit.SECONDS)) {
				// Construct a task to save the results and put it in the queue to get run by
				// the executor.
				Callable<Boolean> saveTask = this.populatorFactory.getSaveTask(results);
				ListenableFuture<Boolean> future = this.saveExecutor.submit(saveTask);
				Futures.addCallback(future, this.populatorFactory.getSaveTaskCallback(this.getSaveTaskSupervisor()),
						this.saveExecutor);
				this.reportingTaskList.add(future);
			}
		} catch (InterruptedException e) {
			this.logger.error("Interrupted Exception occurred while executing method saveResults", e);
		} finally {
			if (this.lock.isHeldByCurrentThread())
				this.lock.unlock();
		}
	}

}
