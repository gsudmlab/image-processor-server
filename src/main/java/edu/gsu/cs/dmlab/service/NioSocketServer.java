/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service;

import java.io.IOException;
import java.net.InetSocketAddress;

import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import edu.gsu.cs.dmlab.config.ConfigReader;

import edu.gsu.cs.dmlab.factories.PopulatorFactory;
import edu.gsu.cs.dmlab.factories.interfaces.IPopulatorFactory;

/**
 * The class that starts listening for connection requests to this process and
 * then handles those requests by passing them on to the completion handler.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class NioSocketServer {

	public NioSocketServer(ConfigReader config) throws IOException {

		// Create the factory for all the objects we will need.
		IPopulatorFactory populatorFactory = new PopulatorFactory(config);

		// Create an AsynchronousServerSocketChannel that will listen on
		// port number set by the config file.
		int portNum = config.getPortNum();
		final AsynchronousServerSocketChannel listener = AsynchronousServerSocketChannel.open()
				.bind(new InetSocketAddress(portNum));

		// Get the connection handler to handle connection requests
		CompletionHandler<AsynchronousSocketChannel, Void> handler = populatorFactory.getCompletionHandler(listener);

		// Start handling new requests
		listener.accept(null, handler);
	}

	public void run() throws InterruptedException {
		// Run forever.
		Thread.currentThread().join();
	}
}
