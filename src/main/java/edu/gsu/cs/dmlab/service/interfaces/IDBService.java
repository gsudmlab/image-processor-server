/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service.interfaces;

import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.ParamResults;

/**
 * The public interface of the class that will handle providing image
 * descriptors to return to processing worker processes, and then save the
 * results that are returned from those processes.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface IDBService {

	/**
	 * The method to get the next descriptor of the next image that needs to be
	 * processed.
	 * 
	 * @return The descriptor of the next image to process.
	 */
	public ParamId getNext();

	/**
	 * The method that saves the results returned from the worker processes.
	 * 
	 * @param results The results to save in the database.
	 */
	public void saveResults(ParamResults results);
}
