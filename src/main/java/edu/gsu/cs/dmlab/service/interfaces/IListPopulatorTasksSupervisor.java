/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service.interfaces;

import org.joda.time.DateTime;

/**
 * The public interface of the class that will handle the callback from list
 * populator tasks that are performed by this process.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface IListPopulatorTasksSupervisor {

	/**
	 * The method that is called when the populate task has completed.
	 * 
	 * @param done The last datetime processed at the successful end of the populate
	 *             task.
	 */
	public void handleParamIdListPopulated(DateTime done);

	/**
	 * The method that is called when the populate task throws an exception of some
	 * sort.
	 * 
	 * @param arg0 The exception that was thrown, probably wrapped in another from
	 *             the executor.
	 */
	public void handleParamIdListPopulatedFailed(Throwable arg0);

}
