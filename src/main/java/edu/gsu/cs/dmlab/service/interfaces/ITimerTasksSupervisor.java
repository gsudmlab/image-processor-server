/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service.interfaces;

/**
 * The public interface of the class that will handle the callback from the
 * delay task that are performed by this process to delay execution of another
 * run populate task.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface ITimerTasksSupervisor {

	/**
	 * The method that is called when the delay processing task has completed.
	 * 
	 * @param done A simple yes/no indicator as to the success of the delay
	 *             processing task.
	 */
	public void handleDelayProcessComplete(Boolean done);

	/**
	 * The method that is called when the delay processing task throws an exception
	 * of some sort.
	 * 
	 * @param arg0 The exception that was thrown, probably wrapped in another from
	 *             the executor.
	 */
	public void handleDelayProcessFailed(Throwable arg0);

}
