/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.datatypes;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.imageio.ImageIO;

import org.joda.time.DateTime;

/**
 * Class used to send the results back from the parameter calculation worker
 * processes.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ParamResults implements Serializable {

	private static final long serialVersionUID = -8847755330307559160L;

	/**
	 * The ID in the tables used to store these results.
	 */
	public int id;

	/**
	 * The X position of the center of the sun within the image.
	 */
	public double X0;

	/**
	 * The Y position of the center of the sun within the image.
	 */
	public double Y0;

	/**
	 * The observed Radius of the sun.
	 */
	public double R_SUN;

	/**
	 * The reference Diameter of the sun.
	 */
	public double DSUN;

	/**
	 * The number of arcseconds per pixel in the full resolution image.
	 */
	public double CDELT;

	/**
	 * The quality bit as defined by the data provider.
	 */
	public int QUALITY;

	/**
	 * The date and time of the image that these results represent.
	 */
	public DateTime time;

	/**
	 * The calculated parameters returned from the parameter calculation worker
	 * process.
	 */
	public double[][][] parameters;

	/**
	 * A buffered image representing the data that was processed.
	 */
	public transient BufferedImage img;

	private void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		ImageIO.write(this.img, "png", out);
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		this.img = ImageIO.read(in);
	}

}
