/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 *public interface of the class that will handle providing image
 * descriptors to return to processing worker processes, and then save the
 * results that are returned from those processes. You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.Callable;

import org.joda.time.Interval;

import edu.gsu.cs.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.ISTImageDBConnection;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.ParamResults;

/**
 * The task that performs the saving of results that are returned by the worker
 * processes.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ResultsSavingTask implements Callable<Boolean> {

	ParamResults result;
	IImageDBCreator dbCreator;
	ISTImageDBConnection dbConnect;
	int cadence;

	/**
	 * Constructor
	 * 
	 * @param result    The result that is to be saved.
	 * 
	 * @param dbConnect The database connection object used to check if the results
	 *                  are already in the database.
	 * 
	 * @param dbCreator The database connection object that is used to insert the
	 *                  results into the database.
	 * 
	 * @param cadence   The expected cadence between image reports in the database.
	 */
	public ResultsSavingTask(ParamResults result, ISTImageDBConnection dbConnect, IImageDBCreator dbCreator,
			int cadence) {
		this.result = result;
		this.dbCreator = dbCreator;
		this.dbConnect = dbConnect;
		this.cadence = cadence;
	}

	@Override
	public Boolean call() {
		try {
			Interval period = new Interval(this.result.time.getMillis(),
					this.result.time.getMillis() + this.cadence * 1000);

			ImageDBFitsHeaderData header = this.dbConnect.getHeaderForId(period, this.result.id);
			if (header == null) {
				header = new ImageDBFitsHeaderData();
				header.CDELT = this.result.CDELT;
				header.DSUN = this.result.DSUN;
				header.R_SUN = this.result.R_SUN;
				header.X0 = this.result.X0;
				header.Y0 = this.result.Y0;
				header.QUALITY = this.result.QUALITY;
				this.dbCreator.insertHeader(header, this.result.id, period);
			}
			// Check if params not exist
			if (!this.dbCreator.checkParamsExist(this.result.id, period))
				this.dbCreator.insertParams(this.result.parameters, this.result.id, period);

			// Check if images not exist
			if (!this.dbCreator.checkImagesExist(this.result.id, period))
				this.dbCreator.insertImage(this.result.img, this.result.id, period);

			header = null;
			period = null;

		} catch (SQLException | IOException e) {
			// Don't really want to do much with this exception.
			return Boolean.FALSE;
		} finally {
			this.result.parameters = null;
			this.result.img.flush();
			this.result.img = null;
			this.result = null;
		}

		return Boolean.TRUE;
	}

}
