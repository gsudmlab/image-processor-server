/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.slf4j.Logger;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.gsu.cs.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.ISTImageDBConnection;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageDataSource;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageFileDataSource;
import edu.gsu.cs.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.util.Utility;

/**
 * This class is used to prepare the database for insertion of processed image
 * data and populates the list used to send processing tasks to the worker
 * process. It checks for missing image parameters/images in the database and
 * then determines if the raw data needed for processing the missing data is
 * present on disk. If not on disk, it will download the data and save it to
 * disk, then placing the descriptor of the missing data into the processing
 * task list. If there is no data to download, it will then remove the
 * descriptor from the database so we don't have descriptors that are missing
 * their data.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class DBAndFileCheckingHelioviewerPullingRunPopulateTask implements Callable<DateTime> {

	private IImageDBCreator dbCreator;
	private ISTImageDBConnection dbConnect;
	private Waveband[] wavelengths;
	private List<ParamId> idsList;
	private IImageFileDataSource downloadDataSource;
	private IImageDataSource diskDataSource;
	private DateTime epoc;
	private int cadence;
	private String baseDir;

	private Logger logger;

	private ListeningExecutorService executor;

	/**
	 * Constructor
	 * 
	 * @param dbCreator   The object used to create tables and insert values in the
	 *                    database.
	 * 
	 * @param dbConnect   The object used to retrieve values from various tables in
	 *                    the database.
	 * 
	 * @param dataSource  The object used to download images from the Helioviewer
	 *                    Datasource.
	 * 
	 * @param wavelengths An array of the wavelengths to check and insert
	 *                    descriptors for in the database.
	 * 
	 * @param idsList     The list that is used to contain the identifiers of images
	 *                    that need to be processed.
	 * 
	 * @param epoc        The start date and time of the month that is to be
	 *                    processed by this task object.
	 * 
	 * @param baseDir     The base directory of where the image files are stored
	 * 
	 * @param numThreads  The number of threads to use while executing checks and
	 *                    downloads
	 * 
	 * @param cadence     The cadence rate in seconds
	 * 
	 * @param logger      A logging mechanism so we can see what is going on
	 */
	public DBAndFileCheckingHelioviewerPullingRunPopulateTask(IImageDBCreator dbCreator, ISTImageDBConnection dbConnect,
			IImageFileDataSource downloadDataSource, IImageDataSource diskDataSource, Waveband[] wavelengths,
			List<ParamId> idsList, DateTime epoc, String baseDir, int numThreads, int cadence, Logger logger) {
		if (dbCreator == null)
			throw new IllegalArgumentException("IImageDBCreator cannot be null in RunPopulateTask constructor.");
		if (dbConnect == null)
			throw new IllegalArgumentException("IImageDBConnection cannot be null in RunPopulateTask constructor.");
		if (downloadDataSource == null)
			throw new IllegalArgumentException("IImageFileDataSource cannot be null in RunPopulateTask constructor.");
		if (diskDataSource == null)
			throw new IllegalArgumentException("IImageDataSource cannot be null in RunPopulateTask constructor.");
		if (wavelengths == null)
			throw new IllegalArgumentException("Waveband Arr cannot be null in RunPopulateTask constructor.");
		if (idsList == null)
			throw new IllegalArgumentException("ParamId List cannot be null in RunPopulateTask constructor.");
		if (epoc == null)
			throw new IllegalArgumentException("Epoc Time cannot be null in RunPopulateTask constructor.");
		if (baseDir == null)
			throw new IllegalArgumentException("Base Directory cannot be null in RunPopulateTask constructor.");

		this.dbCreator = dbCreator;
		this.dbConnect = dbConnect;
		this.downloadDataSource = downloadDataSource;
		this.diskDataSource = diskDataSource;
		this.wavelengths = wavelengths;
		this.idsList = idsList;
		this.epoc = epoc;
		this.baseDir = baseDir;
		this.cadence = cadence;
		this.logger = logger;

		this.executor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(numThreads));
	}

	@Override
	public void finalize() throws Throwable {
		this.dbConnect = null;
		this.dbCreator = null;
		this.wavelengths = null;
		this.idsList = null;
		this.downloadDataSource = null;
		this.diskDataSource = null;
		this.epoc = null;
		this.executor.shutdownNow();
		this.executor = null;
	}

	/**
	 * This class does most of the actual work checking for
	 * files/descriptors/inserting where needed.
	 */
	private class GetImageId implements Callable<ParamId> {

		private IImageDBCreator dbCreator;
		private ISTImageDBConnection dbConnect;
		private IImageFileDataSource downloadDataSource;
		private IImageDataSource diskDataSource;

		private Interval period;
		private Waveband wavelength;
		private String baseDir;

		private Logger logger;

		public GetImageId(ISTImageDBConnection dbConnect, IImageDBCreator dbCreator,
				IImageFileDataSource downloadDataSource, IImageDataSource diskDataSource, Interval period,
				Waveband wavelength, String baseDir, Logger logger) {
			this.dbCreator = dbCreator;
			this.dbConnect = dbConnect;
			this.period = period;
			this.wavelength = wavelength;
			this.downloadDataSource = downloadDataSource;
			this.diskDataSource = diskDataSource;
			this.baseDir = baseDir;
			this.logger = logger;
		}

		@Override
		public ParamId call() throws Exception {

			ParamId result = null;
			try {

				DateTime localStart = new DateTime(this.period.getStart());
				ImageDBDateIdPair[] ids = this.dbConnect.getImageIdsForInterval(this.period, this.wavelength);

				// If no Id falling in that interval for that wavelength, then we need to
				// determine if it is possible to process and then insert a new Id if it is.
				if (ids == null || ids.length == 0) {

					if (this.checkIfFileExists(localStart, this.wavelength)) {
						int id = this.dbCreator.insertFileDescript(this.wavelength, this.period);

						result = new ParamId();
						result.id = id;
						result.time = localStart;
						result.wavelength = this.wavelength;
					}

				} else {

					ImageDBFitsHeaderData header = this.dbConnect.getHeaderForId(ids[0].period, ids[0].id);
					// If null, then we need to process
					if (header == null) {
						if (this.checkIfFileExists(localStart, this.wavelength)) {
							result = new ParamId();
							result.id = ids[0].id;
							result.time = localStart;
							result.wavelength = wavelength;
						}
					} else {
						// If not null, check if parameter and images exist as well.
						if (!this.dbCreator.checkParamsExist(ids[0].id, ids[0].period)
								|| !this.dbCreator.checkImagesExist(ids[0].id, ids[0].period)) {

							if (this.checkIfFileExists(localStart, this.wavelength)) {
								// If they do not exist, then we need to process.
								result = new ParamId();
								result.id = ids[0].id;
								result.time = localStart;
								result.wavelength = wavelength;
							}
						}
					}

				}
			} catch (Exception e) {
				this.logger.error("Error while checking files", e);
			}
			return result;
		}

		private boolean checkIfFileExists(DateTime time, Waveband wave) {
			File f = this.diskDataSource.getImageFile(time, wave);

			if (f == null) {
				Pair<byte[], String> p = this.downloadDataSource.getImageAtTime(time, wave);
				if (p != null) {
					StringBuilder sb = new StringBuilder();
					sb.append(this.baseDir);
					sb.append(File.separator);
					sb.append(time.getYear());
					sb.append(File.separator);
					sb.append(String.format("%02d", time.getMonthOfYear()));
					sb.append(File.separator);
					sb.append(String.format("%02d", time.getDayOfMonth()));
					sb.append(File.separator);
					sb.append(Utility.convertWavebandToInt(wave));
					sb.append(File.separator);
					sb.append(p.getSecond());
					try {
						f = new File(sb.toString());
						FileUtils.forceMkdirParent(f);
						FileUtils.writeByteArrayToFile(f, p.getFirst());
					} catch (Exception e) {
						this.logger.error("Error while attempting to write to disk.", e);
						return false;
					}
					return true;
				} else {
					this.logger.info("Unable to download from source at: " + time.toString() + ", "
							+ Utility.convertWavebandToInt(wave));
					return false;
				}
			} else {
				return true;
			}
		}

	}

	@Override
	public DateTime call() throws Exception {

		DateTime start = new DateTime(this.epoc);
		DateTime end = start.plusMonths(1);
		try {

			// Make sure the tables exist for the entire range we plan to process
			this.dbCreator.insertFileDescriptTables(new Interval(start.getMillis(), start.getMillis() + this.cadence));
			this.dbCreator.insertFileDescriptTables(new Interval(end.getMillis(), end.getMillis() + this.cadence));

			DateTime current = new DateTime();
			if (end.isAfter(current))
				end = current;

			Interval range = new Interval(start, end);
			long numSeconds = range.toDuration().getStandardSeconds();
			long numSteps = numSeconds / this.cadence;

			List<ListenableFuture<ParamId>> taskList = new LinkedList<>();

			for (int stepNum = 0; stepNum < numSteps; stepNum++) {
				DateTime localStart = start.plusSeconds(this.cadence * stepNum);
				Interval period = new Interval(localStart, localStart.plusSeconds(this.cadence));

				for (Waveband wavelength : wavelengths) {
					GetImageId gid = new GetImageId(this.dbConnect, this.dbCreator, this.downloadDataSource,
							this.diskDataSource, period, wavelength, this.baseDir, this.logger);
					ListenableFuture<ParamId> future = this.executor.submit(gid);
					taskList.add(future);
				}
			}

			while (!taskList.isEmpty()) {
				Iterator<ListenableFuture<ParamId>> itr = taskList.iterator();
				while (itr.hasNext()) {
					ListenableFuture<ParamId> tsk = itr.next();
					if (tsk.isDone() || tsk.isCancelled()) {
						itr.remove();
						try {
							if (!tsk.isCancelled()) {
								ParamId id = tsk.get();
								if (id != null)
									this.idsList.add(id);
							}
						} catch (Exception e) {
							this.logger.error("Error while adding ID to list from results of check", e);
						}
					}
				}
				try {
					this.logger.info("Still Processing: " + taskList.size());
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					this.logger.error("Error while waiting for file checking to end.", e);
				}
			}

		} finally {
			this.executor.shutdownNow();
		}

		return end;
	}

}
