/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.config;

import java.io.File;
import java.io.PrintWriter;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.io.IoBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;

/**
 * The configuration file reader class. This class reads the configuration file
 * for this project and provides the objects needed for this project that can be
 * configured at runtime through file changes.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ConfigReader {

	private DataSource imgDBPoolSourc = null;
	private DataSource objDBPoolSourc = null;

	private int pixels;
	private int cellSize;
	private int paramDim;
	private int paramDownSample = 64;
	private int paramCells = 64;
	private int portnum;
	private int maxThreads;
	private int cadence;
	private DateTime epoc;
	private DateTime endTime = null;
	private List<String> blockingProcesses = new ArrayList<String>();
	private String processName;
	private boolean cycle;

	private String baseDir;

	DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

	Waveband[] wavelengths = { Waveband.AIA94, Waveband.AIA131, Waveband.AIA171, Waveband.AIA193, Waveband.AIA211,
			Waveband.AIA304, Waveband.AIA335, Waveband.AIA1600, Waveband.AIA1700 };

	public ConfigReader(String fileLoc, String configName) throws InvalidConfigException {

		this.config(fileLoc, configName);
	}

	public DataSource getImageDBPoolSource() {
		return this.imgDBPoolSourc;
	}

	public DataSource getObjDBPoolSource() {
		return this.objDBPoolSourc;
	}

	public int getPortNum() {
		return this.portnum;
	}

	public int getMaxThreads() {
		return this.maxThreads;
	}

	public int getCadence() {
		return this.cadence;
	}

	public Waveband[] getWavelengths() {
		return this.wavelengths;
	}

	public DateTime getEcpoc() {
		return this.epoc;
	}

	public DateTime getEndTime() {
		return this.endTime;
	}

	public int getParamDownSample() {
		return this.paramDownSample;
	}

	public int getNumParams() {
		return this.paramDim;
	}

	public int getNumCells() {
		return (this.paramCells * this.paramCells);
	}

	public int getHWInCells() {
		return this.paramCells;
	}

	public String getProcessName() {
		return this.processName;
	}

	public List<String> getBlockingProcesses() {
		return this.blockingProcesses;
	}

	public boolean cycleProcessing() {
		return this.cycle;
	}

	public String getBaseDir() {
		return this.baseDir;
	}

	// /////////////////////////////////////////////////////////////////////////////////
	// Start of private methods
	// ////////////////////////////////////////////////////////////////////////////////
	private void config(String folderLocation, String configName) throws InvalidConfigException {
		try {

			DocumentBuilderFactory fctry = DocumentBuilderFactory.newInstance();
			Document doc;
			String fileLoc = folderLocation + File.separator + configName;
			DocumentBuilder bldr = fctry.newDocumentBuilder();
			doc = bldr.parse(new File(fileLoc));
			doc.getDocumentElement().normalize();

			// Used to hold the config for the object pool until after logging config is
			// completed.
			Node imgPoolNode = null;
			Node objPoolNode = null;

			Element root = doc.getDocumentElement();
			NodeList ndLst = root.getChildNodes();

			for (int i = 0; i < ndLst.getLength(); i++) {
				Node nde = ndLst.item(i);
				if (nde.getNodeType() == Node.ELEMENT_NODE) {
					String ndName = nde.getNodeName();
					switch (ndName) {
					case "imagepool":
						imgPoolNode = nde;
						break;
					case "objectpool":
						objPoolNode = nde;
						break;
					case "portnum":
						this.portnum = Integer.parseInt(this.getAttrib(nde, "value"));
						break;
					case "threads":
						this.maxThreads = Integer.parseInt(this.getAttrib(nde, "max"));
						break;
					case "cadence":
						this.cadence = Integer.parseInt(this.getAttrib(nde, "seconds"));
						break;
					case "epoc":
						this.epoc = formatter.parseDateTime(this.getAttrib(nde, "date"));
						break;
					case "endtime":
						this.endTime = formatter.parseDateTime(this.getAttrib(nde, "date"));
						break;
					case "celldepth":
						this.paramDim = Integer.parseInt(this.getAttrib(nde, "value"));
						break;
					case "cellsize":
						this.cellSize = Integer.parseInt(this.getAttrib(nde, "value"));
						break;
					case "imagesize":
						this.pixels = Integer.parseInt(this.getAttrib(nde, "pixels"));
						break;
					case "basedir":
						this.baseDir = this.getAttrib(nde, "value");
						break;
					case "process_name":
						this.processName = this.getAttrib(nde, "name");
						break;
					case "blockingprocesses":
						this.blockingProcesses = this.getBlockingProcessesList(nde.getChildNodes());
						break;
					case "logger":
						folderLocation = this.getAttrib(nde, "loc");
						String logsFileName = this.getAttrib(nde, "name");
						logsFileName = folderLocation + File.separator + logsFileName;
						this.setupLogging(logsFileName);
						break;
					case "cycleprocess":
						this.cycle = Boolean.valueOf(this.getAttrib(nde, "value"));
						break;
					default:
						System.out.print("Unknown Element: ");
						System.out.println(ndName);
					}

				}
			}

			if (imgPoolNode != null) {
				this.imgDBPoolSourc = this.getPoolSourc(imgPoolNode.getChildNodes());
			}

			if (objPoolNode != null) {
				this.objDBPoolSourc = this.getPoolSourc(objPoolNode.getChildNodes());
			}

			this.paramCells = this.pixels / this.cellSize;
			this.paramDownSample = this.cellSize;

		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidConfigException("Config failed with: " + e.getMessage());
		}

	}

	private void setupLogging(String logConfFile) {
		Properties props = System.getProperties();
		props.setProperty("log4j.configurationFile", logConfFile);
		File f = new File(logConfFile);
		URI fc = f.toURI();
		LoggerContext.getContext().setConfigLocation(fc);
	}

	private List<String> getBlockingProcessesList(NodeList ndLst) {
		List<String> blockingList = new ArrayList<String>();
		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {

				case "process":
					String blockingProcess = this.getAttrib(nde, "name");
					blockingList.add(blockingProcess);
					break;
				default:
					System.out.print("Unknown Element: ");
					System.out.println(ndName);
				}
			}
		}
		return blockingList;
	}

	private DataSource getPoolSourc(NodeList ndLst) {
		BasicDataSource dbPoolSourc = new BasicDataSource();

		PrintWriter logger = IoBuilder.forLogger(BasicDataSource.class).setLevel(Level.INFO).buildPrintWriter();

		dbPoolSourc.setPoolPreparedStatements(false);
		dbPoolSourc.setDefaultAutoCommit(true);

		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {

				case "timeout":
					String idlStr = this.getAttrib(nde, "value");
					dbPoolSourc.setDefaultQueryTimeout(Integer.parseInt(idlStr));
					dbPoolSourc.setSoftMinEvictableIdleTimeMillis(Integer.parseInt(idlStr));
					break;
				case "minpool":
					String minStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMinIdle(Integer.parseInt(minStr));
					break;
				case "maxpool":
					String maxStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxIdle(Integer.parseInt(maxStr));
					break;
				case "maxsize":
					String maxszStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxTotal(Integer.parseInt(maxszStr));
					break;
				case "username":
					dbPoolSourc.setUsername(this.getAttrib(nde, "value"));
					break;
				case "password":
					dbPoolSourc.setPassword(this.getAttrib(nde, "value"));
					break;
				case "validationquery":
					dbPoolSourc.setValidationQuery(this.getAttrib(nde, "value"));
					break;
				case "driverclass":
					dbPoolSourc.setDriverClassName(this.getAttrib(nde, "value"));
					break;
				case "url":
					dbPoolSourc.setUrl(this.getAttrib(nde, "value").trim());
					break;
				case "schema":
					dbPoolSourc.setDefaultSchema(this.getAttrib(nde, "value"));
					break;
				default:
					System.out.print("Unknown Element: ");
					System.out.println(ndName);
				}
			}
		}

		try {
			dbPoolSourc.setLogWriter(logger);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return dbPoolSourc;
	}

	private String getAttrib(Node prntNde, String attName) {
		StringBuffer buf = new StringBuffer("");
		boolean isSet = false;
		if (prntNde.hasAttributes()) {
			NamedNodeMap ndeMp = prntNde.getAttributes();
			for (int i = 0; i < ndeMp.getLength(); i++) {
				Node nde = ndeMp.item(i);
				if (nde.getNodeName().compareTo(attName) == 0) {
					buf.append(nde.getNodeValue());
					isSet = true;
					break;
				}
			}
		}

		if (!isSet) {
			return "";
		} else {
			return buf.toString();
		}
	}

}
