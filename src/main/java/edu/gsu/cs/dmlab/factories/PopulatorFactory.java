/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.factories;

import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.gsu.cs.dmlab.config.ConfigReader;
import edu.gsu.cs.dmlab.databases.ImageDBConnection;
import edu.gsu.cs.dmlab.databases.ImageDBCreator;
import edu.gsu.cs.dmlab.databases.NonCacheImageDBConnection;
import edu.gsu.cs.dmlab.databases.Postgres_StateDBConnector;
import edu.gsu.cs.dmlab.databases.Postgres_StateDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBConnector;
import edu.gsu.cs.dmlab.databases.interfaces.IISDStateDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.ISTImageDBConnection;
import edu.gsu.cs.dmlab.datasources.HelioviewerImageFileDatasource;
import edu.gsu.cs.dmlab.datasources.JP2_FileDataSource;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageDataSource;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageFileDataSource;
import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.ParamResults;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.factories.interfaces.IPopulatorFactory;
import edu.gsu.cs.dmlab.service.CyclingServerTaskSupervisor;
import edu.gsu.cs.dmlab.service.OnlineServerTaskSupervisor;
import edu.gsu.cs.dmlab.service.interfaces.IDBSaveTasksSupervisor;
import edu.gsu.cs.dmlab.service.interfaces.IDBService;
import edu.gsu.cs.dmlab.service.interfaces.IListPopulatorTasksSupervisor;
import edu.gsu.cs.dmlab.service.io.ConnectHandler;
import edu.gsu.cs.dmlab.service.io.ReadResultsHandler;
import edu.gsu.cs.dmlab.tasks.DBAndFileCheckingHelioviewerPullingRunPopulateTask;
import edu.gsu.cs.dmlab.tasks.ParamIDListPopulatedCallback;
import edu.gsu.cs.dmlab.tasks.ResultsSavedCallback;
import edu.gsu.cs.dmlab.tasks.ResultsSavingTask;

/**
 * Class used to build the objects required by this project.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class PopulatorFactory implements IPopulatorFactory {

	private IImageDBCreator dbCreator;
	private ISTImageDBConnection dbConnect;
	private IDBService dbService;

	private IImageDataSource diskImgSource;
	private IImageFileDataSource downloadImgSource;

	private Waveband[] wavelengths;
	private int cadence;
	private Logger connectHandlerLogger;

	private String baseDir;
	private int maxThreads;

	private Logger resultHandlerLogger;
	private Logger fileCheckkerLogger;

	/**
	 * Constructor
	 * 
	 * @param config The class that contains all of the configuration information
	 *               read from the config file.
	 */
	public PopulatorFactory(ConfigReader config) {
		this.wavelengths = config.getWavelengths();
		this.cadence = config.getCadence();
		this.baseDir = config.getBaseDir();

		int numParams = config.getNumParams();
		int paramDownSample = config.getParamDownSample();

		DataSource dstor = config.getImageDBPoolSource();

		int correctParamRowCount = config.getNumCells();
		this.maxThreads = config.getMaxThreads();
		int maxConcurrent = (int) (this.maxThreads * 1.75);

		DateTime epoc = config.getEcpoc();
		DateTime endTime = config.getEndTime();

		ListeningExecutorService saveExecutor = MoreExecutors
				.listeningDecorator(Executors.newFixedThreadPool(maxThreads));
		ListeningExecutorService connectionHandleExecutor = MoreExecutors
				.listeningDecorator(Executors.newFixedThreadPool(5));

		Logger imgDBCreatorLogger = LoggerFactory.getLogger(ImageDBCreator.class);
		Logger imgDBConnLogger = LoggerFactory.getLogger(ImageDBConnection.class);
		Logger dbServiceLogger = LoggerFactory.getLogger(IDBService.class);
		Logger dbStateCreatorLogger = LoggerFactory.getLogger(IISDStateDBCreator.class);
		Logger dbStateConnectLogger = LoggerFactory.getLogger(IISDStateDBConnector.class);
		this.connectHandlerLogger = LoggerFactory.getLogger(CompletionHandler.class);
		this.resultHandlerLogger = LoggerFactory.getLogger(ReadResultsHandler.class);
		this.fileCheckkerLogger = LoggerFactory.getLogger(DBAndFileCheckingHelioviewerPullingRunPopulateTask.class);

		this.dbCreator = new ImageDBCreator(dstor, correctParamRowCount, numParams, imgDBCreatorLogger);
		this.dbConnect = new NonCacheImageDBConnection(dstor, null, numParams, paramDownSample, imgDBConnLogger);

		Logger downloadImageSourceLogger = LoggerFactory.getLogger(IImageFileDataSource.class);
		this.downloadImgSource = new HelioviewerImageFileDatasource(maxThreads, config.getCadence(),
				config.getCadence() - 1, downloadImageSourceLogger);

		Logger diskImageSourceLogger = LoggerFactory.getLogger(IImageDataSource.class);
		this.diskImgSource = new JP2_FileDataSource(this.baseDir, 30, config.getCadence(), config.getCadence() / 2,
				diskImageSourceLogger);

		IISDStateDBCreator stateDBCreator = new Postgres_StateDBCreator(config.getObjDBPoolSource(),
				dbStateCreatorLogger);
		IISDStateDBConnector stateDBConnect = new Postgres_StateDBConnector(config.getObjDBPoolSource(),
				config.getBlockingProcesses(), config.getEcpoc(), config.getProcessName(), dbStateConnectLogger);

		if (config.cycleProcessing()) {
			this.dbService = new CyclingServerTaskSupervisor(this, stateDBCreator, stateDBConnect,
					connectionHandleExecutor, saveExecutor, epoc, endTime, maxConcurrent, dbServiceLogger);
		} else {
			long runDelayMilis = 360000;
			this.dbService = new OnlineServerTaskSupervisor(this, stateDBCreator, stateDBConnect,
					connectionHandleExecutor, saveExecutor, epoc, maxConcurrent, runDelayMilis, dbServiceLogger);
		}

	}

	@Override
	public Callable<DateTime> getPopulatorTask(LinkedList<ParamId> idsList, DateTime month) {

		Callable<DateTime> populator = new DBAndFileCheckingHelioviewerPullingRunPopulateTask(this.dbCreator,
				this.dbConnect, this.downloadImgSource, this.diskImgSource, this.wavelengths, idsList, month,
				this.baseDir, this.maxThreads, this.cadence, this.fileCheckkerLogger);

		return populator;
	}

	@Override
	public Callable<Boolean> getSaveTask(ParamResults result) {
		Callable<Boolean> task = new ResultsSavingTask(result, this.dbConnect, this.dbCreator, this.cadence);
		return task;
	}

	@Override
	public FutureCallback<DateTime> getPopulatorTaskCallback(IListPopulatorTasksSupervisor supervisor) {
		FutureCallback<DateTime> callback = new ParamIDListPopulatedCallback(supervisor);
		return callback;
	}

	@Override
	public FutureCallback<Boolean> getSaveTaskCallback(IDBSaveTasksSupervisor supervisor) {
		FutureCallback<Boolean> callback = new ResultsSavedCallback(supervisor);
		return callback;
	}

	@Override
	public CompletionHandler<AsynchronousSocketChannel, Void> getCompletionHandler(
			AsynchronousServerSocketChannel listener) {
		ReadResultsHandler readResultHandler = new ReadResultsHandler(this.dbService, this.resultHandlerLogger);
		return new ConnectHandler(listener, readResultHandler, this.dbService, this.connectHandlerLogger);
	}

}
