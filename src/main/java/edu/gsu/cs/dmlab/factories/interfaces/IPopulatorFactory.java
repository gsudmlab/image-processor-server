/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.factories.interfaces;

import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.LinkedList;
import java.util.concurrent.Callable;

import org.joda.time.DateTime;

import com.google.common.util.concurrent.FutureCallback;

import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.ParamResults;
import edu.gsu.cs.dmlab.service.interfaces.IDBSaveTasksSupervisor;
import edu.gsu.cs.dmlab.service.interfaces.IListPopulatorTasksSupervisor;

/**
 * The public interface for the class that is used to build the objects used by
 * this project.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface IPopulatorFactory {

	/**
	 * Gets a Callable object that performs the task of populating the database with
	 * the descriptors for images that will eventually be processed, and then
	 * returns those descriptors.
	 * 
	 * @param idsList   The linked list that will contain the descriptors of images
	 *                  that need to be processed.
	 * 
	 * @param startDate The start date time for the task to begin running from.
	 * 
	 * @return A callable task that performs the task of populating the database
	 *         with the descriptors for images that will eventually be processed.
	 */
	public Callable<DateTime> getPopulatorTask(LinkedList<ParamId> idsList, DateTime startDate);

	/**
	 * The populator task needs a callback, this method produces a callback object
	 * for the executor to call when the populator task is completed.
	 * 
	 * @param supervisor The task supervisor that will be notified of the callback
	 *                   when the populator is finished processing.
	 * 
	 * @return A FutureCallback object that will be called when the populator is
	 *         finished processing.
	 */
	public FutureCallback<DateTime> getPopulatorTaskCallback(IListPopulatorTasksSupervisor supervisor);

	/**
	 * Gets a Callable object that performs the save task on the returned results
	 * from the processing worker processes.
	 * 
	 * @param result The results to insert into the database.
	 * 
	 * @return A callable task that saves the results to the database.
	 */
	public Callable<Boolean> getSaveTask(ParamResults result);

	/**
	 * The save task needs a callback, this method produces a callback object for
	 * the executor to call when the save task is complete.
	 * 
	 * @param supervisor The task supervisor that will be notified of the callback
	 *                   when the save task is finished processing.
	 * 
	 * @return A FutureCallback object that will be called when the save task is
	 *         finished processing.
	 */
	public FutureCallback<Boolean> getSaveTaskCallback(IDBSaveTasksSupervisor supervisor);

	/**
	 * In order to handle the establishment of a connection to this process, we need
	 * a completion handler to handle when such a connection is established. This
	 * method produces one.
	 * 
	 * @param listener The listener object that listens for connection requests.
	 * 
	 * @return A completion handler that handles when a connection is established
	 *         with this process.
	 */
	public CompletionHandler<AsynchronousSocketChannel, Void> getCompletionHandler(
			AsynchronousServerSocketChannel listener);

}
