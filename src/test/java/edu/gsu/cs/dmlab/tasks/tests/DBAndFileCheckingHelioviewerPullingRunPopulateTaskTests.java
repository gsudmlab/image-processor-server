package edu.gsu.cs.dmlab.tasks.tests;

import static org.mockito.Mockito.mock;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import edu.gsu.cs.dmlab.databases.interfaces.ISTImageDBConnection;
import edu.gsu.cs.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageDataSource;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageFileDataSource;
import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.tasks.DBAndFileCheckingHelioviewerPullingRunPopulateTask;

public class DBAndFileCheckingHelioviewerPullingRunPopulateTaskTests {

	@Test
	public void testConstructorThrowsOnNullImageDBCreator() {
		IImageDBCreator dbCreator = null;// mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = mock(IImageDataSource.class);
		Waveband[] wavelengths = new Waveband[0];
		List<ParamId> idsList = new LinkedList<ParamId>();
		DateTime epoc = new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}

	@Test
	public void testConstructorThrowsOnNullImageDBConnector() {
		IImageDBCreator dbCreator = mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = null;// mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = mock(IImageDataSource.class);
		Waveband[] wavelengths = new Waveband[0];
		List<ParamId> idsList = new LinkedList<ParamId>();
		DateTime epoc = new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}

	@Test
	public void testConstructorThrowsOnNullDataSource() {
		IImageDBCreator dbCreator = mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = null;// mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = mock(IImageDataSource.class);
		Waveband[] wavelengths = new Waveband[0];
		List<ParamId> idsList = new LinkedList<ParamId>();
		DateTime epoc = new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}

	@Test
	public void testConstructorThrowsOnNullDiskDataSource() {
		IImageDBCreator dbCreator = mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = null;// mock(IImageDataSource.class);
		Waveband[] wavelengths = new Waveband[0];
		List<ParamId> idsList = new LinkedList<ParamId>();
		DateTime epoc = new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}

	@Test
	public void testConstructorThrowsOnNullWavebandArr() {
		IImageDBCreator dbCreator = mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = mock(IImageDataSource.class);
		Waveband[] wavelengths = null;// new Waveband[0];
		List<ParamId> idsList = new LinkedList<ParamId>();
		DateTime epoc = new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}

	@Test
	public void testConstructorThrowsOnNullIDsList() {
		IImageDBCreator dbCreator = mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = mock(IImageDataSource.class);
		Waveband[] wavelengths = new Waveband[0];
		List<ParamId> idsList = null;// new LinkedList<ParamId>();
		DateTime epoc = new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}

	@Test
	public void testConstructorThrowsOnNullEpoc() {
		IImageDBCreator dbCreator = mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = mock(IImageDataSource.class);
		Waveband[] wavelengths = new Waveband[0];
		List<ParamId> idsList = new LinkedList<ParamId>();
		DateTime epoc = null;// new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}

	@Test
	public void testConstructorThrowsOnNullBaseDir() {
		IImageDBCreator dbCreator = mock(IImageDBCreator.class);
		ISTImageDBConnection dbConnect = mock(ISTImageDBConnection.class);
		IImageFileDataSource dataSource = mock(IImageFileDataSource.class);
		IImageDataSource diskDataSource = mock(IImageDataSource.class);
		Waveband[] wavelengths = new Waveband[0];
		List<ParamId> idsList = new LinkedList<ParamId>();
		DateTime epoc = new DateTime();
		Logger logger = mock(Logger.class);
		String baseDir = null;// "";
		int maxThreads = 1;
		int cadecne = 360;

		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new DBAndFileCheckingHelioviewerPullingRunPopulateTask(dbCreator, dbConnect, dataSource, diskDataSource,
					wavelengths, idsList, epoc, baseDir, maxThreads, cadecne, logger);
		});
	}
}
